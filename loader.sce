// ====================================================================
// Copyright Stephane MOTTELET 2023
// This file is released into the public domain
// ====================================================================

if atomsIsInstalled("scicolpack")
    mprintf("ColPack interface is now part of Scilab, removing module:\n");
    disp(atomsRemove("scicolpack"))
    mprintf("\nPlease restart Scilab.\n");
end
